# ATM App - Front-end Client

Please copy this repository and run following commands in the terminal:

```
npm install

npm start
```

The ATM App will start and be available on the address: http://localhost:3000
