import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class App extends Component {

  constructor(props){
    super(props);

    // set default state values
    this.state = {
      amount: '',
      notesToWithdraw: []
    }
  }

  addNumberToAmount(value){
    this.setState({ amount: this.state.amount + value });
  }

  withdrawCash(){
    this.setState({ notesToWithdraw: [] });
    fetch(`http://localhost:3001/withdraw/${this.state.amount}`, { mode: 'cors'})
      .then(body => {
        if (!body.ok){
          // it's an ATM - let's not show any debug info to the client
          this.setState({ amount: '' });
          return false;
        }
        return body.json();
      })
      .then(response => {
        if (response) {
          this.setState({ amount: '', notesToWithdraw: response });
        }
      });
  }

  renderNotes(){
    let animationDelay = -1;
    return (this.state.notesToWithdraw
      && this.state.notesToWithdraw.map((value, index) => {
        animationDelay++;
        return (
          <div
            key={`note${index}-${value}`}
            className={`note`}
            style={{'animationDelay': animationDelay + 's'}}>
              $ {value}
          </div>
        );
    }));
  }

  renderPad(){
    const padNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
    return (
      <div>
        <div className="pad">
        { padNumbers.map(item =>
          <button onClick={() => {
             this.addNumberToAmount(item);
           }}
           key={`padNumber${item}`}
           className='button'>{item}</button>
          )
        }
        </div>
        <button
          className='button-withdraw-cash'
          onClick={() => this.withdrawCash()}>Withdraw cash</button>
      </div>
    );
  }

  render(){
    return(
      <div className="atm">
        <div className="logo">Virtual ATM</div>

        <div className="atm-screen">
          {this.state.amount ? this.state.amount : 'Please enter the amount of money to withdraw'}
        </div>

        {this.renderPad()}

        <div className="cash-container">
          <div className="cash-slot" />
          { this.renderNotes() }
        </div>

      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
