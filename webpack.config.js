const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: {
    app: './src/index.js'
  },
  devtool: 'inline-source-map',
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: false
    })
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },

  module: {
   rules: [
     {
       test: /\.css$/,
       use: [ 'style-loader', 'css-loader' ]
     },
     {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
   ]
 },

  mode: process.env.NODE_ENV,

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 9000,
    open: true,
    inline: true,
    watchContentBase: true
  }
};
